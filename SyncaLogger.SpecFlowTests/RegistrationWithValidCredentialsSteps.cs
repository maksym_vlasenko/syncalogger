﻿using Newtonsoft.Json;
using NUnit.Framework;
using SyncaLogger.Api.Models;
using SyncaLogger.Tests;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SyncaLogger.SpecFlowTests
{
    [Binding]
    public class RegistrationWithValidCredentialsSteps
    {
        private readonly ScenarioContext scenarioContext;
        private readonly HttpClient client;

        public RegistrationWithValidCredentialsSteps(ScenarioContext scenarioContext)
        {
            client = new TestProvider().Client;

            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");
            this.scenarioContext = scenarioContext;
        }

        [Given(@"New user valid credentials")]
        public void GivenNewUserValidCredentials()
        {
            RegisterModel user = new RegisterModel()
            {
                UserName = "SpecFlowLogin",
                Password = "tralala1"
            };

            scenarioContext.Set(user, "user");
        }
        
        [When(@"I create post request to Api with user credentials")]
        public async Task WhenICreatePostRequestToApiWithUserCredentials()
        {
            var user = scenarioContext.Get<RegisterModel>("user");
            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"/api/account/registration", data);

            scenarioContext.Set(response, "registerResponse");
        }
        
        [Then(@"Return created status code")]
        public void ThenReturnCreatedStatusCode()
        {
            var expectedStatusCode = System.Net.HttpStatusCode.Created;

            var response = scenarioContext.Get<HttpResponseMessage>("registerResponse");

            var statusCode = response.StatusCode;

            Assert.AreEqual(statusCode, expectedStatusCode);
        }
    }
}
