﻿using Newtonsoft.Json;
using NUnit.Framework;
using SyncaLogger.Api.Models;
using SyncaLogger.SpecFlowTests.Infrastructure;
using SyncaLogger.Tests;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SyncaLogger.SpecFlowTests
{
    [Binding]
    public class GetTestSteps : DBMethodsForTesting
    {
        private readonly ScenarioContext scenarioContext;
        private readonly HttpClient client;
        public GetTestSteps(ScenarioContext scenarioContext)
        {
            client = new TestProvider().Client;

            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");
            this.scenarioContext = scenarioContext;
        }

        [Given(@"User login into his account")]
        public async Task GivenUserLoginIntoHisAccount()
        {
            await RegisterTestUser(client);
            await SeedLogsToDB(client);

            var user = new LoginModel()
            {
                UserName = "testlogin",
                Password = "Testpass"
            };

            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"/api/account/login", data);

            scenarioContext.Add("loginResponse", response);
        }

        [Given(@"login is success")]
        public async Task GivenLoginIsSuccess()
        {
            HttpResponseMessage response;

            scenarioContext.TryGetValue("loginResponse", out response);

            Assert.True(response.IsSuccessStatusCode);

            var realJwt = await response.Content.ReadAsStringAsync();

            scenarioContext.Add("jwt", realJwt);
        }

        [When(@"User request Api for log by Id")]
        public async Task WhenUserRequestApiForLogById()
        {
            scenarioContext.TryGetValue("jwt", out var jwt);

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwt);

            var response = await client.GetAsync($"/api/log/1");

            scenarioContext.Add("getResponse", response);
        }

        [Then(@"Response must be with Ok StatusCode")]
        public async Task ThenResponseMustBeWithOkStatusCode()
        {
            var expectedStatusCode = System.Net.HttpStatusCode.OK;

            scenarioContext.TryGetValue("getResponse", out HttpResponseMessage response);

            var statusCode = response.StatusCode;

            Assert.AreEqual(expectedStatusCode, statusCode);

        }  
    }
}
