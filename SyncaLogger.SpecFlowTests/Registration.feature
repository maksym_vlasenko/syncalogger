﻿Feature: Registration
	As an user of Api
	I Want to create new account

@mytag
Scenario: Registration with valid credentials
	Given New user valid credentials
	When I create post request to Api with user credentials
	Then Return created status code

	Scenario: Registration if user already exist
	Given User already exist with 
	| Field           | Value     |
	| UserName        | testlogin |
	| Password        | Testpass  |
	| CinfirmPassword | Testpass  |
	When I register user with the same credentials
	Then Response returns bad request status code
