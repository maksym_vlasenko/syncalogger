﻿using Newtonsoft.Json;
using NUnit.Framework;
using SyncaLogger.Api.Models;
using SyncaLogger.Tests;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SyncaLogger.SpecFlowTests
{
    [Binding]
    public class RegistrationIfUserAlreadyExistSteps
    {
        private readonly ScenarioContext scenarioContext;
        private readonly HttpClient client;

        public RegistrationIfUserAlreadyExistSteps(ScenarioContext scenarioContext)
        {
            client = new TestProvider().Client;

            if (scenarioContext == null) throw new ArgumentNullException("scenarioContext");
            this.scenarioContext = scenarioContext;
        }

        [Given(@"User already exist with")]
        public async Task GivenUserAlreadyExistWith(Table table)
        {
            var user = table.CreateInstance<RegisterModel>();

            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync($"/api/account/registration", data);

            scenarioContext.Set(user, "existedUser");
        }
        
        [When(@"I register user with the same credentials")]
        public async Task WhenIRegisterUserWithTheSwmaCredentials()
        {
            var user = scenarioContext.Get<RegisterModel>("existedUser");

            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync($"/api/account/registration", data);

            scenarioContext.Set(response, "registerResponse");
        }
        
        [Then(@"Response returns bad request status code")]
        public void ThenResponseReturnsStatusCode()
        {
            var expectedStatusCode = System.Net.HttpStatusCode.BadRequest;

            var response = scenarioContext.Get<HttpResponseMessage>("registerResponse");

            var statusCode = response.StatusCode;

            Assert.AreEqual(statusCode, expectedStatusCode);
        }
    }
}
