﻿
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace SyncaLogger.Sink
{
    public static class SinkExtension
    {
        public static LoggerConfiguration UseSyncaSink(
                  this LoggerSinkConfiguration loggerConfiguration,
                  IHttpClientFactory httpFactory,
                  IConfiguration config,
                  IFormatProvider formatProvider = null)
        {
            return loggerConfiguration.Sink(new SyncaSink (httpFactory, config,formatProvider));
        }
    }
}
