﻿using System;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace SyncaLogger.Sink
{
    public class SyncaSink : ILogEventSink, IDisposable
    {
        private SyncaBuffer _buffer;
        private readonly IFormatProvider _formatProvider;
        public SyncaSink(IHttpClientFactory service, IConfiguration config1, IFormatProvider formatProvider)
        {
            _buffer = new SyncaBuffer(service, config1);
            _formatProvider = formatProvider;
        }
        public void Emit(LogEvent logEvent)
        {
            Task t = Task.Run(async () =>
            {
                await _buffer.Write(new SyncaLog(logEvent.RenderMessage(_formatProvider), logEvent.Level, DateTime.Now));
            });
        }
        public async void Dispose()
        {
          await _buffer.Dispose();
        }
    }
}
