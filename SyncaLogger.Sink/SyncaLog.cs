﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;
using Serilog.Events;

namespace SyncaLogger.Sink
{
    public class SyncaLog
    {
        public string Text { get; set; }
        public LogEventLevel Level { get; set; } 
        public DateTime Date { get; set; }
        public SyncaLog(string text, LogEventLevel level,DateTime date)
        {
            Text = text;
            Level = level;
            Date = date;
        }
        public SyncaLog()
        {
        }
    }
}
