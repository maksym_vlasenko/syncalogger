﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using SyncaLogger.Api.Contacts;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace SyncaLogger.Sink
{
    public class SyncaBuffer
    {
        private static CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private CancellationToken token = cancelTokenSource.Token;
        private ConcurrentQueue<SyncaLog> _buffer;
        private readonly IMapper _iMapper;
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _client;
        private bool disposed = false;

        public SyncaBuffer(IHttpClientFactory client, IConfiguration config1)
        {
            _client = client;
            _config = config1;
            _buffer = new ConcurrentQueue<SyncaLog>();

            Task t = Task.Run(async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    await Task.Delay(2000);
                    await WriteBufferAsync();
                   // token.ThrowIfCancellationRequested();
                }
                await WriteBufferAsync();
            }, token);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SyncaLog, TransferLogModel>();
            });
            _iMapper = config.CreateMapper();
        }

        //Send logs to API
        public async Task WriteBufferAsync()
        {
            List<TransferLogModel> transitBuff = new List<TransferLogModel>();

            while (_buffer.TryDequeue(out SyncaLog log))
            {
                // TODO: AutoMapper
                var transferModel = _iMapper.Map<TransferLogModel>(log);
                transitBuff.Add(transferModel);
            }

            string json = JsonConvert.SerializeObject(transitBuff);

            transitBuff.Clear();

            // TODO: HttpClientFactory
            using (var httpClient = _client.CreateClient())
            {
                httpClient.BaseAddress = new Uri(_config["ConnectionPath"]);
                httpClient.DefaultRequestHeaders.Add("ApiKey", _config["ApiKey"]);

                var data = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await httpClient
                .PostAsync("", data);

                response.EnsureSuccessStatusCode();

                string result = await response.Content.ReadAsStringAsync();

                Console.WriteLine(result);
            }
        }
        public async Task Write(SyncaLog log)
        {
            _buffer.Enqueue(log);

            if (_buffer.Count > 10)
                await WriteBufferAsync();
        }




        public async Task Dispose()
        {
            cancelTokenSource.Cancel();
        }
    }
}
