﻿using System;
using System.Collections.Generic;
using System.Text;
using Serilog.Events;

namespace SyncaLogger.Data.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public LogEventLevel Level { get; set; }
        public DateTime Date { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
