﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyncaLogger.Data.Options
{
    public class JwtOptions
    {
        public string Key { get; set; }
        public int Expires { get; set; }
    }
}
