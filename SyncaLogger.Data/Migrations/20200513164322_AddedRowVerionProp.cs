﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SyncaLogger.Data.Migrations
{
    public partial class AddedRowVerionProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "RowVersion",
                table: "Logs",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RowVersion",
                table: "Logs");
        }
    }
}
