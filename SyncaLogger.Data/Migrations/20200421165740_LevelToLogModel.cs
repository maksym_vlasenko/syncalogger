﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SyncaLogger.Data.Migrations
{
    public partial class LevelToLogModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Num",
                table: "Logs");

            migrationBuilder.AddColumn<int>(
                name: "Level",
                table: "Logs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Level",
                table: "Logs");

            migrationBuilder.AddColumn<int>(
                name: "Num",
                table: "Logs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
