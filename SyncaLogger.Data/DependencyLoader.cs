﻿using Microsoft.Extensions.DependencyInjection;
using SyncaLogger.Data.Repositories;
using SyncaLogger.Data.Repositories.Default;

namespace SyncaLogger.Data
{
    public static class DependencyLoader
    {
        public static void Load(IServiceCollection services)
        {
            services.AddScoped<ILogRepository, LogRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
