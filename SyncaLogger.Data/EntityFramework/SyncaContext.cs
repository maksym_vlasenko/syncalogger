﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SyncaLogger.Data.Models;

namespace SyncaLogger.Data.EntityFramework
{
    public class SyncaContext : IdentityDbContext<SyncaUser>
    {
        public virtual DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Log>()
                .Property(c => c.RowVersion)
                .IsRowVersion();

            base.OnModelCreating(modelBuilder);
        }

        public SyncaContext(DbContextOptions<SyncaContext> options)
            : base(options)
        {
        }

        public SyncaContext()
            : base()
        {
        }
    }
}
