﻿using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Data.Repositories
{
    public interface ILogRepository
    {
        Task AddAsync(Log value);
        Task AddRangeAsync(IEnumerable<Log> value);
        Task<Log> GetByIdAsync(int id);
        IQueryable<Log> GetQuery();
    }
}
