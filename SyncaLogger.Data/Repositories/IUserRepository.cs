﻿using Microsoft.AspNetCore.Identity;
using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Data.Repositories
{
    public interface IUserRepository
    {
        Task<IdentityResult> AddUserAsync(SyncaUser value,string password);
        Task<SyncaUser> GetUserByUserName(string userName);
        Task<SignInResult> SignInAsync(string userName, string password);

    }
}
