﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SyncaLogger.Data.EntityFramework;
using SyncaLogger.Data.Models;

namespace SyncaLogger.Data.Repositories.Default
{
    public class LogRepository : ILogRepository
    {
        private readonly SyncaContext _db;
        public LogRepository(SyncaContext db)
        {
            _db = db;
        }
        public async Task AddAsync(Log value)
        {
            await _db.Logs.AddAsync(value);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<Log> value)
        {
            await _db.Logs.AddRangeAsync(value);
            await _db.SaveChangesAsync();
        }

        public async Task<Log> GetByIdAsync(int id)
        {
            return await _db.Logs.FirstOrDefaultAsync(p => p.Id == id);
        }

        public IQueryable<Log> GetQuery()
        {
            return _db.Logs.AsQueryable();
        }
    }
}
