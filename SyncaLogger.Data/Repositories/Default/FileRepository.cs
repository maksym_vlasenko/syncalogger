﻿using Microsoft.Extensions.Configuration;
using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Data.Repositories.Default
{
    internal class FileRepository : IFileRepository
    {
        private readonly IConfiguration _config;

        public FileRepository(IConfiguration config)
        {
            _config = config;
        }

        public async Task WriteToFileAsync(IEnumerable<Log> logs)
        {
            string fileName = $"{_config["FilePath"]}{DateTime.Now.ToShortDateString()}_log.log";

           await using (StreamWriter sw = new StreamWriter(
                fileName,
                true,
                System.Text.Encoding.Default
            ))

            {
                List<Task> tasks = new List<Task>();

                foreach (var item in logs)
                {
                    tasks.Add(sw.WriteLineAsync($"{item.Date} || {item.Text} || {item.Level}"));
                }

                await sw.WriteLineAsync("\n");
                await Task.WhenAll(tasks);
            }
        }
    }
}
