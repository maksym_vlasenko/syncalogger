﻿using Microsoft.AspNetCore.Identity;
using SyncaLogger.Data.EntityFramework;
using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Data.Repositories.Default
{
    public class UserRepository : IUserRepository
    {
        private readonly SyncaContext _db;
       private readonly SignInManager<SyncaUser> _signInManager;
        private readonly UserManager<SyncaUser> _userManager;

        public UserRepository(SyncaContext db, 
            UserManager<SyncaUser> userManager,
            SignInManager<SyncaUser> signInManager){
            _signInManager = signInManager;
            _userManager = userManager;
            _db = db;

        }

        public async Task<IdentityResult> AddUserAsync(SyncaUser value,string password)
        {           
          return await _userManager.CreateAsync(value,password);
        }

        public async Task<SignInResult> SignInAsync(string userName, string password)
        {
           return await _signInManager.PasswordSignInAsync(userName,password,false,false);
        }

        public async Task<SyncaUser> GetUserByUserName(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }
    }
}
