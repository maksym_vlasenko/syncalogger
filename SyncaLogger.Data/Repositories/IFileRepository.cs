﻿using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Data.Repositories
{
    public interface IFileRepository
    {
        Task WriteToFileAsync(IEnumerable<Log> logs);
    }
}
