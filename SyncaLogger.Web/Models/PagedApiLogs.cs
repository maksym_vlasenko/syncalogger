﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Web.Models
{
    public class PagedApiLogs
    {
        public IEnumerable<ApiLog> logDTOs { get; set; }
        public int TotalCount { get; set; }
    }
}
