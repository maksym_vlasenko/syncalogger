﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SyncaLogger.Web.Models
{
    public class ApiLog
    {
        [Display(Name = "Message")]
        public string Text { get; set; }

        [Display(Name = "Log level")]
        public SyncaLogLevel Level { get; set; }

        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        
    }
}
