﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Web.Models
{
    public class SearchFilter
    {
        [Display(Name = "Sort ASC?")]
        public bool SortType { get; set; } = true;

        [Display(Name = "Log level")]
        public SyncaLogLevel LogLevel { get; set; } = SyncaLogLevel.All;

        [Display(Name = "Choose logs after this Date")]
        public DateTime AfterThisDate { get; set; } = DateTime.Now.AddDays(-25);

        [Display(Name = "Choose logs before this Date")]
        public DateTime BeforeThisDate { get; set; } = DateTime.Now;

    }

    public enum SyncaLogLevel
    {
        Verbose = 0,
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5,
        All = 6
    }
}
