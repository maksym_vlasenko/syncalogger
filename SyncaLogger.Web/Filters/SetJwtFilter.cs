﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Web.Filters
{
    public class SetJwtFilter : IAsyncResourceFilter
    {
        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            if (context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT") != null)
            {
              
                var jwtSaved = context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT").Value;
                context.HttpContext.Request.Headers.Add("Authorization", "Bearer " + jwtSaved);
                await next();
              

              
            }
            else
            {
                await next();
            }

        }
    }
}
