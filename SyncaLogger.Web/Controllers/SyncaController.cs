﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AspNet.Security.OAuth.GitLab;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using SyncaLogger.Web.Models;
using X.PagedList;

namespace SyncaLogger.Web.Controllers
{
    public class SyncaController : Controller
    {
        // GET: Log

        private readonly IHttpClientFactory _httpClient;

        public SyncaController(IHttpClientFactory httpClient)
        {
            _httpClient = httpClient;
           
        }


        [Authorize]
        public async Task<ActionResult> LogById(int id)
        {

            using (var client = _httpClient.CreateClient())
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/");

                if (HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT") != null)
                {

                    var jwtSaved = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT").Value;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtSaved);
                }

                var response = await client.GetAsync($"log/{id}");

                string jsonResult = await response.Content.ReadAsStringAsync();

                ApiLog logModel = JsonConvert.DeserializeObject<ApiLog>(jsonResult);

                return View(logModel);
            }

        }

       
        [HttpGet]
       // [Authorize]
        public async Task<ActionResult> Search(SearchFilter filter,int p=1,int s = 10)
        {
            UriBuilder builder = new UriBuilder("https://localhost:5001/api/log");

            ViewBag.Page = p;
            ViewBag.Limit = s;
            builder.Query = $"page={p}" +
                $"&pagelimit={s}" +
                $"&after={filter.AfterThisDate.ToString("yyyyMMddHHmmss")}" +
                $"&before={filter.BeforeThisDate.ToString("yyyyMMddHHmmss")}" +
                $"&level={filter.LogLevel}" +
                $"&asc={filter.SortType}";

            using (var client = _httpClient.CreateClient())
            {
                if (HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT") != null)
                {

                    var jwtSaved = HttpContext.User.Claims.FirstOrDefault(x => x.Type == "JWT").Value;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtSaved);
                }

                var response = await client.GetAsync(builder.Uri);

                if (!response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    return Content(result);
                }

                string jsonResult = await response.Content.ReadAsStringAsync();

                var logModel = JsonConvert.DeserializeObject<PagedApiLogs>(jsonResult);

                return View(logModel);
            }
        }


        //[HttpPost]
        //public async Task<ActionResult> Search(SearchFilter filter)
        //{
        //    using (var client = _httpClient.CreateClient())
        //    {
        //        UriBuilder builder = new UriBuilder("https://localhost:5001/api/log");

        //        builder.Query = $"page={filter.Page}" +
        //            $"&pagelimit={filter.PageLimit}" +
        //            $"&after={filter.AfterThisDate.ToString("yyyyMMddHHmmss")}" +
        //            $"&before={filter.BeforeThisDate.ToString("yyyyMMddHHmmss")}" +
        //            $"&level={filter.LogLevel}" +
        //            $"&asc={filter.SortType}";

        //        var response = await client.GetAsync(builder.Uri);

        //        if (!response.IsSuccessStatusCode)
        //        {
        //            string result = await response.Content.ReadAsStringAsync();
        //            return Content(result);
        //        }

        //        string jsonResult = await response.Content.ReadAsStringAsync();

        //        var logModel = JsonConvert.DeserializeObject<IEnumerable<ApiLog>>(jsonResult).AsQueryable();

        //        return View(logModel);
        //    }
        //}
    }
}