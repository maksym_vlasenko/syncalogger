﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SyncaLogger.Web.Models.Account;

namespace SyncaLogger.Web.Controllers
{
    
    public class AccountController : Controller
    {

        private readonly IHttpClientFactory _clientFactory;

        public AccountController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet]
        public async Task<IActionResult> Register()
        {

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> LogOut()
        {

            await HttpContext.SignOutAsync();

            return RedirectToAction("Search","Synca");
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel user)
        {
            if (!ModelState.IsValid)
                return View(user);

            using (var client = _clientFactory.CreateClient())
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/account/registration");

                string json = JsonConvert.SerializeObject(user);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync("", data);


                if(response.IsSuccessStatusCode)
                    return RedirectToAction("Login");

            }

            return BadRequest("Error during registration");
        }
        
        public async Task<IActionResult> GitLabLogin()
        {

            return Challenge(new AuthenticationProperties { RedirectUri = "/" }, "GitLab");
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel user)
        {
            if (!ModelState.IsValid)
                return View(user);

            string personalJwt;

            using (var client = _clientFactory.CreateClient())
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/account/login");

                string json = JsonConvert.SerializeObject(user);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync("", data);

                personalJwt = await response.Content.ReadAsStringAsync();

            }

            //
            var claims = new List<Claim>
            {
                new Claim("JWT", personalJwt)

            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {

            };

                    await HttpContext.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            new ClaimsPrincipal(claimsIdentity),
            authProperties);
 
            return RedirectToAction("Search","Synca");
        }
    }
}