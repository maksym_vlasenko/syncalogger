﻿using FluentValidation;
using SyncaLogger.Api.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Validation
{
    public class LogValidator: AbstractValidator<TransferLogModel>
    {
        public LogValidator()
        {
            RuleFor(a => a.Text)
                .NotEmpty();

            RuleFor(a => a.Date)
                .NotEmpty()
                .LessThan(DateTime.Now);

        }
    }
}
