﻿using FluentValidation;
using Serilog.Events;
using SyncaLogger.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Validation
{
    public class SearchFilterValidator : AbstractValidator<SearchFilter>
    {
        public SearchFilterValidator()
        {
            RuleFor(a => a.AfterThisDate)
                .LessThanOrEqualTo(DateTime.Now);

            RuleFor(a => a.LogLevel)
                .IsInEnum();

            RuleFor(a=>a.PageLimit)
                .GreaterThanOrEqualTo(10);

            RuleFor(a => a.Page)
                .GreaterThanOrEqualTo(1);

            RuleFor(a => a.BeforeThisDate)
                .GreaterThan(a=>a.AfterThisDate);
        }
    }
}
