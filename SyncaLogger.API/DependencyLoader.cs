﻿using Microsoft.Extensions.DependencyInjection;
using SyncaLogger.Domain;

namespace SyncaLogger.Api
{
    internal static class DependencyLoader
    {
        public static void Load(this IServiceCollection services)
        {
            services.AddBusinessLogicLayer();
        }
    }
}
