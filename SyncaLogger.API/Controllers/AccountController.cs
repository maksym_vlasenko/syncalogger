﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Domain.Services;
using SyncaLogger.Data.Models;
using SyncaLogger.Api.Models;
using Microsoft.AspNetCore.Authentication;

namespace SyncaLogger.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUserService _users;

        public AccountController(IConfiguration config, IUserService users)
        {
            _config = config;
            _users = users;
        }

        /// <summary>
        /// Register new User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     api\Registration
        ///       {
        ///        "username" : "testlogin"
        ///        "password": "Testpass",
        ///        }
        /// </remarks>
        /// <response code="201">User registered</response> 
        /// <response code="400">Bad request</response> 
        [HttpPost("Registration")]
        public async Task<IActionResult> Post(RegisterModel model)
        {

            SyncaUserDTO user = new SyncaUserDTO()
            {
                UserName = model.UserName,
                Password = model.Password
            };

            try
            {
                var result = await _users.CreateUser(user);
                return StatusCode(201, result.ToString());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Login and get JWT.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     api\Login
        ///       {
        ///        "username" : "testlogin"
        ///        "password": "Testpassword",
        ///        }
        /// </remarks>
        /// <response code="200">New istance of JWT</response> 
        /// <response code="400">Invalid login or password</response> 
        [HttpPost("Login")]
        public async Task<object> Post(LoginModel model)
        {
            SyncaUserDTO dto = new SyncaUserDTO()
            {
                UserName = model.UserName,
                Password = model.Password
            };

            var result = await _users.SignIn(dto);

            if (result.Succeeded)
            {
                var user = await _users.FindUserByName(dto.UserName);
                return await _users.GetJWT(user);
            }

            return BadRequest(result.ToString());
        }
    }
}