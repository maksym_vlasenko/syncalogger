﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Api.Filters;
using SyncaLogger.Api.Contacts;
using SyncaLogger.Domain.Services;
using Microsoft.AspNetCore.Authorization;
using System;
using Microsoft.AspNetCore.Http;
using SyncaLogger.Api.Models;
using System.Linq;
using SyncaLogger.Api.Validation;
using System.Globalization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace SyncaLogger.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
   // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LogController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFileService _fileWriter;
        private readonly ILogService _dbService;

        public LogController(IFileService fileWriter,
            IMapper mapper,
            ILogService dbService)
        {
            _fileWriter = fileWriter;
            _mapper = mapper;
            _dbService = dbService;
        }

        /// <summary>
        /// Gets all Logs from DB.
        /// </summary>
        /// <response code="200">List of logs</response> 
        /// <response code="400">If filter is uncorrect</response> 
        /// <response code="401">If JWT doesn't correct</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<IEnumerable<LogDTO>>> Get([FromQuery] SearchFilter filter)
        {

            var validator = new SearchFilterValidator();
            var validationResult = await validator.ValidateAsync(filter);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            try
            {
                var filterDTO = _mapper.Map<SearchFilterDTO>(filter);

                var logs = await _dbService.Find(filterDTO);
                return Ok(logs);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Gets Log from DB by ID.
        /// </summary>
        /// <response code="200">Instance of log</response> 
        /// <response code="204">If Id not found</response> 
        /// <response code="401">If JWT doesn't correct</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("{id}", Name = "Get")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Get(int id)
        {

            LogDTO log = await _dbService.GetLogById(id);

            if (log == null)
            {
                return NoContent();
            }

            return Ok(log);
        }

        /// <summary>
        /// Write list of logs into DB.
        /// </summary>
        /// <remarks>
        /// <param name="postLog"></param>
        /// Sample request:
        ///
        ///     api\log
        ///     {
        ///      [
        ///       {
        ///        "text" : "Error message",
        ///        "level": 1,
        ///        "Date": "2020-05-10T15:07:29.125"
        ///        },
        ///        {
        ///        "text" : "Error message",
        ///        "level": 3,
        ///        "Date": "2020-05-12T19:24:29.678"
        ///        }
        ///       [ 
        ///     }
        ///
        /// </remarks>
        ///  <returns>"Good!"</returns>
        /// <response code="201">Returns Good!</response>
        /// <response code="400">If the logs are null</response> 
        /// <response code="401">If ApiKey doesn't correct</response>  
        /// <response code="500">Server error</response>  
        [ApiKeyAuthorization]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Post(IEnumerable<TransferLogModel> postLog)
        {

            if (postLog == null)
                return BadRequest(postLog);

            int k = postLog.Count();

            var logDto = _mapper.Map<IEnumerable<TransferLogModel>,
                IEnumerable<LogDTO>>(postLog);

            try
            {
                await _dbService.SaveRange(logDto);
                return StatusCode(201, $"Good! {k}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [ApiKeyAuthorization]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> PostFile(IEnumerable<TransferLogModel> postLog)
        {
            if (postLog == null)
                return BadRequest();

            var logDto = _mapper.Map<IEnumerable<TransferLogModel>,
                IEnumerable<LogDTO>>(postLog);

            await _fileWriter.WriteLogToFileAsync(logDto);

            return Ok("Good!");
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult> Hello()
        {

            return Ok("Hello");
        }
    }
}
