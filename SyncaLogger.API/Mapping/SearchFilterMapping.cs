﻿using AutoMapper;
using SyncaLogger.Api.Models;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Mapping
{
    public class SearchFilterMapping: Profile
    {
        public SearchFilterMapping()
        {
            CreateMap<SearchFilter, SearchFilterDTO>()
                .ForPath(x=>x.PageOptions.PageNumber,x=>x.MapFrom(y=>y.Page))
                .ForPath(x => x.PageOptions.PageSize, x => x.MapFrom(y => y.PageLimit));
        }
    }
}
