﻿using AutoMapper;
using SyncaLogger.Api.Contacts;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Mapping
{
    public class LogMapping: Profile
    {
        public LogMapping()
        {
            CreateMap<TransferLogModel, LogDTO>()
                .ForMember(x=>x.Level,x=>x.MapFrom(y=> (SyncaLogLevel)y.Level));  
        }
    }
}
