﻿using AutoMapper;
using SyncaLogger.Api.Models;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Mapping
{
    public class UserMapping : Profile
    {
        public UserMapping()
        {
            CreateMap<RegisterModel, SyncaUserDTO>();
            CreateMap<LoginModel, SyncaUserDTO>();
        }      
    }
}
