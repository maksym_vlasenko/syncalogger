using AspNet.Security.OAuth.GitLab;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SyncaLogger.Api.Infrastructure;
using SyncaLogger.Api.Middlewares;
using SyncaLogger.Data.EntityFramework;
using SyncaLogger.Data.Models;
using SyncaLogger.Data.Options;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace SyncaLogger.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(x =>
           {  
               x.RequireHttpsMetadata = false;

               x.SaveToken = true;
               x.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidIssuer = "Me",
                   ValidAudience = "My Api",
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Jwt:Key"])),
                   ValidateIssuer = false,
                   ValidateAudience = false,     
               };
           });

            services.AddControllers(opts=>
            {
                opts.ModelBinderProviders.Insert(0, new SyncaDateTimeModelBinderProvider());
            });

            services.AddDbContext<SyncaContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("SyncaConnection")));

            services.AddIdentity<SyncaUser, IdentityRole>()
            .AddEntityFrameworkStores<SyncaContext>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            services.Load();

            services.Configure<JwtOptions>(Configuration.GetSection("Jwt"));

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Synca",
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "Maksym Vlasenko",
                        Email = "randomemail@gmail.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Demo",
                        Url = new Uri("https://example.com/license"),
                    }
                }
                );
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);

            });

            services.AddAutoMapper(typeof(Startup), typeof(SyncaLogger.Domain.BisMap));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Synca V1");
            });



            app.UseHttpsRedirection();
            app.UseMiddleware<LoggingMiddleware>();
            app.UseMiddleware<SetAuthHeadreMiddleware>();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
