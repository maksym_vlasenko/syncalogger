﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Middlewares
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public LoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this._next = next;
            _logger = loggerFactory.CreateLogger<LoggingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            _logger.LogDebug(
                "Request {method} || {url} || {ContentType}",
                context.Request?.Method,
                context.Request?.Path.Value, context.Request.ContentType);

            await _next(context);

            _logger.LogDebug(
                "Response => {statusCode}",
                context.Response?.StatusCode);
        }

    }
}
