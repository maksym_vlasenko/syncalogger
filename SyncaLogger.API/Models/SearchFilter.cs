﻿using Microsoft.AspNetCore.Mvc;
using Serilog.Events;
using SyncaLogger.Domain.DTOModels;
using System;


namespace SyncaLogger.Api.Models
{
    public class SearchFilter
    {
        [FromQuery(Name = "level")]
        public SyncaLogLevel LogLevel { get; set; }
        [FromQuery(Name = "after")]
        public DateTime AfterThisDate { get; set; }
        [FromQuery(Name = "before")]
        public DateTime BeforeThisDate { get; set; }

        [FromQuery(Name = "asc")]
        public bool SortType { get; set; }

        [FromQuery(Name = "Page")]
        public int Page { get; set; } = 1;

        [FromQuery(Name = "PageLimit")]
        public int PageLimit { get; set; } = 10;

      //  [FromQuery(Name = "after")]
        public string AfterDate { get; set; }

     //   [FromQuery(Name = "before")]
        public string BeforeDate { get; set; }
    }

}
