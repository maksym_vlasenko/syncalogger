﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Filters
{
    public class ApiKeyAuthorization : Attribute,IAsyncResourceFilter
    {
        public async  Task OnResourceExecutionAsync(
            ResourceExecutingContext context,
            ResourceExecutionDelegate next) {

            const string ApiKeyHeader = "ApiKey";

            if (!context.HttpContext.Request.Headers.
                TryGetValue(ApiKeyHeader,out var potentialApiKey))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var config = context.HttpContext.RequestServices.
                GetRequiredService<IConfiguration>();
            var apiKey = config["ApiKey"];

            if (!apiKey.Equals(potentialApiKey))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            await next();
        }
    }
}
