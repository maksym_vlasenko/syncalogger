﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SyncaLogger.Api.Models;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SyncaLogger.Api.Infrastructure
{
    public class SyncaDateTimeModelBinder : IModelBinder
    {

        private readonly IModelBinder fallbackBinder;
        public SyncaDateTimeModelBinder(IModelBinder fallbackBinder)
        {
            this.fallbackBinder = fallbackBinder;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var name = bindingContext.ModelName;
            var value = bindingContext.ValueProvider.GetValue(name);

            if (value == ValueProviderResult.None)
                return fallbackBinder.BindModelAsync(bindingContext);

            string stringDate = value.FirstValue;

            if ((DateTime.TryParseExact(stringDate,
                 "yyyyMMddHHmmss",
                 CultureInfo.InvariantCulture,
                  DateTimeStyles.None, out var parsedDate)))
                {

                bindingContext.Result = ModelBindingResult.Success(parsedDate);

            }
            else
            {
                throw new ArgumentException("Cannot parse date");
            }
            return Task.CompletedTask;
        }
    }

}
