﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyncaLogger.Domain.DTOModels
{
    public class SyncaUserDTO
    {
       // public string Id { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }

        public int Age { get; set; }
    }
}
