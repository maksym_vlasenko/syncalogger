﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyncaLogger.Domain.DTOModels
{
    public enum SyncaLogLevel
    {
        Verbose = 0,
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5,
        All = 6
    }
}
