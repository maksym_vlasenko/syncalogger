﻿using SyncaLogger.Data.Models;
using System;


namespace SyncaLogger.Domain.DTOModels
{
   public class SearchFilterDTO
    {
        
        public SyncaLogLevel LogLevel { get; set; }       
        
        public DateTime AfterThisDate { get; set; }

        public DateTime BeforeThisDate { get; set; }

        public Pagination PageOptions { get; set; }

        public bool SortType { get; set; }
    }
}
