﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyncaLogger.Domain.DTOModels
{
    public class PagedLogsDTO
    {
        public int TotalCount { get; set; }
        public IEnumerable<LogDTO> logDTOs { get; set; }
    }
}
