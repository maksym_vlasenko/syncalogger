﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SyncaLogger.Domain.DTOModels
{
    public class LogDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Text { get; set; }
        public SyncaLogLevel Level { get; set; }
        public DateTime Date { get; set; }
       
    }
}
