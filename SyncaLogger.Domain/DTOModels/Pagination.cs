﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyncaLogger.Domain.DTOModels
{
    public class Pagination
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}
