﻿using AutoMapper;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Data.Models;


namespace SyncaLogger.Domain
{
    public class BisMap : Profile
    {
        public BisMap()
        {
            CreateMap<LogDTO, Log>();
            
            CreateMap<Log, LogDTO>()
                  .ForMember(x => x.Level, x => x.MapFrom(y => (SyncaLogLevel)y.Level));
        }
    }
}
