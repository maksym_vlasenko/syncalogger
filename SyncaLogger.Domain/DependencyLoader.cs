﻿using Microsoft.Extensions.DependencyInjection;
using SyncaLogger.Domain.Services.Default;
using SyncaLogger.Domain.Services;
using SyncaLogger.Domain.Default.Services;

namespace SyncaLogger.Domain
{
    public static class DependencyLoader
    {
        public static void AddBusinessLogicLayer(this IServiceCollection services)
        {
            
            Data.DependencyLoader.Load(services);
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<IUserService, UserService>();

        }
    }
}
