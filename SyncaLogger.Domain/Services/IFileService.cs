﻿using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Domain.Services
{
    public interface IFileService
    {
        Task WriteLogToFileAsync(IEnumerable<LogDTO> logs);
    }
}
