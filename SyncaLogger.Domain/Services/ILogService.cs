﻿using SyncaLogger.Data.Models;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SyncaLogger.Domain.Services
{
    public interface ILogService
    {
        Task SaveRange(IEnumerable<LogDTO> log);
        Task<LogDTO> GetLogById(int id);
        Task<PagedLogsDTO> Find(SearchFilterDTO filter);

      
    }
}
