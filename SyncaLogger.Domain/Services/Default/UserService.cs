﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SyncaLogger.Data.Models;
using SyncaLogger.Data.Options;
using SyncaLogger.Data.Repositories;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Domain.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Domain.Default.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _users;
        private readonly IOptions<JwtOptions> _jwtOptions;

        public UserService(IUserRepository users, IOptions<JwtOptions> jwtOptions)
        {
            _users = users;
            _jwtOptions = jwtOptions;
        }

        public async Task<IdentityResult> CreateUser(SyncaUserDTO user)
        {

            SyncaUser syncaUser = new SyncaUser()
            { 
                UserName= user.UserName,
            };

            if(await _users.GetUserByUserName(user.UserName)==null)
            {
                return await _users.AddUserAsync(syncaUser, user.Password);
            }
            else
            {
                throw new Exception("User already exist");
            }
           
        }

        public async Task<SignInResult> SignIn(SyncaUserDTO user)
        { 

            return await _users.SignInAsync(user.UserName,user.Password);
        }

        public async Task<SyncaUserDTO> FindUserByName(string userName)
        {
            var syncaUser = await _users.GetUserByUserName(userName);

            return new SyncaUserDTO() {UserName=syncaUser.UserName };
        }

        public async Task<object> GetJWT(SyncaUserDTO user)
        {
            var tokenTimeCreated = DateTime.UtcNow;

            var claims = new List<Claim>
                {
                    new Claim("UserName", user.UserName)
                };

            string secretKey = _jwtOptions.Value.Key;
            int expires = _jwtOptions.Value.Expires;

            var jwt = new JwtSecurityToken(
                    issuer: "Account controller",
                    audience: "Log controller",
                    notBefore: tokenTimeCreated,
                    claims: claims,
                    expires: tokenTimeCreated.AddMinutes(expires),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(secretKey)),
                        SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
    }
}
