﻿using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using SyncaLogger.Data.Repositories;
using SyncaLogger.Domain.Services;

namespace SyncaLogger.Domain.Services.Default
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        private readonly IMapper _mapper;

        public FileService(IFileRepository fileRepository, IMapper mapper)
        {
            _fileRepository = fileRepository;
            _mapper = mapper;
        }

        public async Task WriteLogToFileAsync(IEnumerable<LogDTO> logs)
        {
            var map = _mapper.Map<IEnumerable<LogDTO>, IEnumerable<Log>>(logs);
            await _fileRepository.WriteToFileAsync(map);
        }
    }
}
