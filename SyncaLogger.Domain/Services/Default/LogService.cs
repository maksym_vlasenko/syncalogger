﻿using SyncaLogger.Data.Models;
using SyncaLogger.Data.Repositories;
using System.Threading.Tasks;
using SyncaLogger.Domain.DTOModels;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;

namespace SyncaLogger.Domain.Services.Default
{
    public class LogService : ILogService
    {
        private readonly ILogRepository _repo;
        private readonly IMapper _mapper;

        public LogService(ILogRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task SaveRange(IEnumerable<LogDTO> log)
        {
            var map = _mapper.Map<IEnumerable<LogDTO>, IEnumerable<Log>>(log);

            await _repo.AddRangeAsync(map);
        }

        public async Task<LogDTO> GetLogById(int id)
        {
            Log temp = await _repo.GetByIdAsync(id);
            LogDTO dto = _mapper.Map<LogDTO>(temp);

            return dto;
        }

        

        public async Task<PagedLogsDTO> Find(SearchFilterDTO filter)
        {
            var result = _repo.GetQuery();

            if (filter.LogLevel != SyncaLogLevel.All)
            {
                result = result
                    .Where(u => u.Level == (LogEventLevel)filter.LogLevel);
            }

            result = result
                 .Where(u => u.Date > filter.AfterThisDate);

            result = result
                 .Where(u => u.Date < filter.BeforeThisDate);

            if (filter.SortType)
            {
                result = result
                    .OrderByDescending(u => u.Date);
            }
            else
            {
                result = result
                    .OrderBy(u => u.Date);
            }

            int count = await result.CountAsync();

            //Pagination
            result = result
                .Skip((filter.PageOptions.PageNumber - 1) * filter.PageOptions.PageSize)
                .Take(filter.PageOptions.PageSize);

            var logs = await result
                .ToListAsync();

            var logDTOs = _mapper
                .Map<IEnumerable<Log>, IEnumerable<LogDTO>>(logs);

            PagedLogsDTO pagedDTOs = new PagedLogsDTO()
            {
                TotalCount = count,
                logDTOs = logDTOs
            };

            return pagedDTOs;
        }

        public Task<PagedLogsDTO> ToPaged(IEnumerable<LogDTO> logs, int count)
        {
            throw new System.NotImplementedException();
        }
    }
}
