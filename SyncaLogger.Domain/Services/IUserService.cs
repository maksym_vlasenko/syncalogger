﻿using Microsoft.AspNetCore.Identity;
using SyncaLogger.Domain.DTOModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Domain.Services
{
    public interface IUserService
    {
        Task<IdentityResult> CreateUser(SyncaUserDTO user);
        Task<SignInResult> SignIn(SyncaUserDTO user);
        Task<SyncaUserDTO> FindUserByName(string userName);
        Task<object> GetJWT(SyncaUserDTO user);

    }
}
