﻿using Serilog;
using System;
using SyncaLogger.Sink;
using Serilog.Events;
using SyncaLogger.LogGenerator.Options;
using CommandLine;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System.IO;

namespace SyncaLogger.LogGenerator
{
    class Program
    {
        static ServiceProvider serviceProvider;
        static IConfiguration config;
        static async Task Main()
        {
            serviceProvider = new ServiceCollection()
                .AddHttpClient()
                .BuildServiceProvider();

            config = new ConfigurationBuilder()
               .AddJsonFile("SyncaConfig.json", true, true)
               .Build();


            Console.WriteLine(config["ConnectionPath"]);

            //Settings for multy thread mode
            var args = @"multy -l Random -t 1 --thread 1 -i 650".Split();
            try
            {
                await CommandLine.Parser.Default.ParseArguments<InstantMode, IntervalMode, MultyThreadMode>(args)
                    .MapResult(
                      (InstantMode opts) => RunInstantLogMod(opts),
                      (IntervalMode opts) => RunIntervalLogMod(opts),
                      (MultyThreadMode opts) => RunMultyIntervalLog(opts),
                      errs => Task.FromResult(1));
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine($"{nameof(OperationCanceledException)} thrown with message: {e.Message}");
            }

            Console.ReadLine();
        }
        public static async Task<int> RunInstantLogMod(InstantMode opts)
        {
            Serilog.Core.Logger log = new LoggerConfiguration()
               .MinimumLevel.Information()
               .WriteTo.UseSyncaSink(
                serviceProvider.GetRequiredService<IHttpClientFactory>(),
                config)
               .CreateLogger();

            Task task = Task.Run(() =>
            {
                if (opts.Level == SyncaLogLevel.Random)
                {
                    Random rand = new Random();
                    int x;
                    for (int i = 0; i < opts.Count; i++)
                    {
                        x = rand.Next(0, 6);
                        log.Write(GetRandomLogLevel(x), "Instant Log {0} / {1}", i, opts.Count);
                    }
                }
                else
                {
                    for (int i = 0; i < opts.Count; i++)
                    {
                        log.Write((LogEventLevel)opts.Level, "Instant Log {0} / {1}", i, opts.Count);
                    }
                }
            });

            await task;

            return 1;
        }
        public static async Task<int> RunIntervalLogMod(IntervalMode opts)
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            cancelTokenSource.CancelAfter(opts.Time * 60000);
            CancellationToken token = cancelTokenSource.Token;

            Serilog.Core.Logger log = new LoggerConfiguration()
               .MinimumLevel
               .Information()
               .WriteTo
               .UseSyncaSink(serviceProvider.GetRequiredService<IHttpClientFactory>(), config)
               .CreateLogger();

            Task task = Task.Run(async () =>
            {
                token.ThrowIfCancellationRequested();
                if (opts.Level == SyncaLogLevel.Random)
                {
                    Random rand = new Random();
                    while (!token.IsCancellationRequested)
                    {
                        int x = rand.Next(0, 6);
                        log.Write(GetRandomLogLevel(x), "Interval Log ");
                        await Task.Delay(opts.Interval);
                        token.ThrowIfCancellationRequested();
                    }
                }
                else
                {
                    while (!token.IsCancellationRequested)
                    {
                        log.Write((LogEventLevel)opts.Level, "Interval Log ");
                        await Task.Delay(opts.Interval);
                        token.ThrowIfCancellationRequested();
                    }
                    Console.WriteLine("End of Work!");
                }
            }, token);

            await task;
            log.Dispose();

            return 1;
        }
        public static async Task<int> RunMultyIntervalLog(MultyThreadMode opts)
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            cancelTokenSource.CancelAfter(opts.Time * 10000);
            CancellationToken token = cancelTokenSource.Token;

            using (Serilog.Core.Logger log = new LoggerConfiguration()
               .MinimumLevel
               .Information()
               .WriteTo
               .UseSyncaSink(serviceProvider.GetRequiredService<IHttpClientFactory>(), config)
               .CreateLogger())
            {

                Task[] tasks = new Task[opts.ThreadCount];
                for (int i = 0; i < tasks.Length; i++)
                    tasks[i] = Task.Run(async () =>
                    {
                        if (opts.Level == SyncaLogLevel.Random)
                        {
                            Random rand = new Random();
                            while (!token.IsCancellationRequested)
                            {
                                int x = rand.Next(0, 6);
                                log.Write(GetRandomLogLevel(x), "Multy Interval Mode || {0}  || ", Thread.CurrentThread.ManagedThreadId);
                                await Task.Delay(opts.Interval);
                                token.ThrowIfCancellationRequested();
                            }
                        }
                        else
                        {
                            while (!token.IsCancellationRequested)
                            {
                                log.Write((LogEventLevel)opts.Level, "Multy Interval Mode || {0}  || ", Thread.CurrentThread.ManagedThreadId);
                                await Task.Delay(opts.Interval);
                                token.ThrowIfCancellationRequested();
                            }
                        }
                    }, token);

                await Task.WhenAll(tasks);
            }

            return 1;
        }

        private static LogEventLevel GetRandomLogLevel(int randNumber)
        {
            return randNumber switch
            {
                0 => LogEventLevel.Verbose,
                1 => LogEventLevel.Debug,
                2 => LogEventLevel.Error,
                3 => LogEventLevel.Fatal,
                4 => LogEventLevel.Information,
                5 => LogEventLevel.Warning
            };
        }
    }
}
