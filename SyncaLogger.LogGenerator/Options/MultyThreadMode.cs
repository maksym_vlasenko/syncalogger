﻿using System;
using System.Collections.Generic;
using System.Text;
using Serilog.Events;
using CommandLine;

namespace SyncaLogger.LogGenerator.Options
{
    [Verb("multy", HelpText = "Write logs in (t) threads.")]
    public class MultyThreadMode
    {

        [Option('i', "interval", Required = true, HelpText = "Interval.")]
        public int Interval { get; set; }

        [Option('l', "level", Required = true, HelpText = "Log level.")]
        public SyncaLogLevel Level { get; set; }

        [Option("thread", Required = true, HelpText = "Count of threads")]
        public int ThreadCount { get; set; }

        [Option('t', "time", Required = true, HelpText = "Working time in minutes")]
        public int Time { get; set; }

    }
}
