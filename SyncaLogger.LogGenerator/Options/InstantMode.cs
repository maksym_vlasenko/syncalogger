﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;
using Serilog.Events;

namespace SyncaLogger.LogGenerator.Options
{
    [Verb("instant", HelpText = "Write (n) log instant.")]
    public class InstantMode
    {
        [Option('c', "count", Required = true, HelpText = "Count of logs.")]
        public int Count { get; set; }

        [Option('l', "level", Required = true, HelpText = "Log level.")]
        public SyncaLogLevel Level { get; set; }
    }
}
