﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;
using Serilog.Events;
using SyncaLogger.LogGenerator;

namespace SyncaLogger.LogGenerator.Options
{
    [Verb("interval", HelpText = "Write logs with interval (i).")]
    public class IntervalMode
    {
        
        [Option('i', "interval", Required = true, HelpText = "Interval.")]
        public int Interval { get; set; }

        [Option('l', "level", Required = true, HelpText = "Log level.")]
        public SyncaLogLevel Level { get; set; }

        [Option('t', "time", Required = true, HelpText = "Working time in minutes")]
        public int Time { get; set; }
    }
}
