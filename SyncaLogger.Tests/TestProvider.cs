﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using SyncaLogger.Api;
using SyncaLogger.Api.Models;
using SyncaLogger.Data.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Tests
{
    public class TestProvider : IDisposable
    {
        private TestServer server;
        public HttpClient Client { get; private set; }
        public TestProvider()
        {
            IWebHostBuilder builder = new WebHostBuilder()
               .UseContentRoot(Path.Combine(GetSolutionBasePath(), "SyncaLogger.Tests"))
               .ConfigureServices(services=>
               {
                   services.RemoveAll(typeof(SyncaContext));
                   services.AddDbContext<SyncaContext>(options =>
                   {
                       options.UseInMemoryDatabase("TestDB");
                   }); 
                 
               })
               .UseEnvironment("Development")
               .UseConfiguration(new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build()
                ).UseStartup<Startup>();

            server = new TestServer(builder);

            Client = server.CreateClient();
        }
        public static string GetSolutionBasePath()
        {
            var appPath = PlatformServices.Default.Application.ApplicationBasePath;
            var binPosition = appPath.IndexOf("\\bin", StringComparison.Ordinal);
            var basePath = appPath.Remove(binPosition);

            var backslashPosition = basePath.LastIndexOf("\\", StringComparison.Ordinal);
            basePath = basePath.Remove(backslashPosition);
            return basePath;
        }

        public void Dispose()
        {
            server?.Dispose();
            Client?.Dispose();
        }
    }
}
