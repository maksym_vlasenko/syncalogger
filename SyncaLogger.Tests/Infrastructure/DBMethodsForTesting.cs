﻿using Newtonsoft.Json;
using Serilog.Events;
using SyncaLogger.Api.Contacts;
using SyncaLogger.Api.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SyncaLogger.Tests.Infrastructure
{
    public class DBMethodsForTesting
    {
        public async Task<object> GetTestJwt(HttpClient client)
        {
            await RegisterTestUser(client);

            var user = new LoginModel()
            {
                UserName = "testlogin",
                Password = "Testpass"
            };

            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"/api/account/login", data);

            var jwt = await response.Content.ReadAsStringAsync();

            response.EnsureSuccessStatusCode();
            return jwt;
        }

        public static async Task RegisterTestUser(HttpClient client)
        {
            var user = new RegisterModel()
            {
                UserName = "testlogin",
                Password = "Testpass",
                ConfirmPassword = "Testpass"
            };

            string json = JsonConvert.SerializeObject(user);

            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync($"/api/account/registration", data);
        }

        public static async Task SeedLogsToDB(HttpClient client)
        {
            var logs = new List<TransferLogModel>()
            {
                 new TransferLogModel() {Text = "Error message 1",Date=DateTime.Now, Level = LogEventLevel.Information},
                new TransferLogModel() { Text = "Error message 2",Date=DateTime.Now.AddDays(-10), Level = LogEventLevel.Warning },
                new TransferLogModel() { Text = "Error message 3",Date=DateTime.Now.AddDays(-1), Level = LogEventLevel.Fatal },
                new TransferLogModel() {Text = "Error message 4",Date=DateTime.Now.AddDays(-5), Level = LogEventLevel.Information }
            };

            string json = JsonConvert.SerializeObject(logs);

            var data = new StringContent(json, Encoding.UTF8, "application/json");
            client.DefaultRequestHeaders.Add("ApiKey", "1144apiSynca11");

            var response = await client.PostAsync($"/api/log", data);
        }
    }
}
