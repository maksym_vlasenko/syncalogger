﻿using FluentValidation.TestHelper;
using SyncaLogger.Api.Models;
using SyncaLogger.Api.Validation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SyncaLogger.Tests
{
    public class SearchFilterTests
    {
        [Fact]
        public void SearchFilterInvalidAfterDate()
        {
            //
            var searchFilter = new SearchFilter() { AfterThisDate = DateTime.Now.AddDays(1) };

            var validator = new SearchFilterValidator();

            //
            var result = validator.TestValidate(searchFilter);

            //
            result.ShouldHaveValidationErrorFor(t => t.AfterThisDate);
        }

        [Fact]
        public void SearchFilterValidAfterDate()
        {
            //
            var searchFilter = new SearchFilter() { AfterThisDate = DateTime.Now.AddDays(-1) };

            var validator = new SearchFilterValidator();

            //
            var result = validator.TestValidate(searchFilter);

            //
            result.ShouldNotHaveValidationErrorFor(t => t.AfterThisDate);
        }

        [Fact]
        public void SearchFilterValidLogLevel()
        {
            //
            var searchFilter = new SearchFilter() { LogLevel = Domain.DTOModels.SyncaLogLevel.Information };

            var validator = new SearchFilterValidator();

            //
            var result = validator.TestValidate(searchFilter);

            //
            result.ShouldNotHaveValidationErrorFor(t => t.LogLevel);
        }

        [Fact]
        public void InvalidResultWhenItemsPerPageEqual0()
        {
            //
            var searchFilter = new SearchFilter() { PageLimit = 0 };

            var validator = new SearchFilterValidator();

            //
            var result = validator.TestValidate(searchFilter);

            //
            result.ShouldHaveValidationErrorFor(t => t.PageLimit);
        }

        [Fact]
        public void InvalidResultWhenItemsPerPageLessThan0()
        {
            //
            var searchFilter = new SearchFilter() { PageLimit = -1 };

            var validator = new SearchFilterValidator();

            //
            var result = validator.TestValidate(searchFilter);

            //
            result.ShouldHaveValidationErrorFor(t => t.PageLimit);
        }
    }
}
