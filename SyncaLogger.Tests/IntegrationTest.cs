﻿using Newtonsoft.Json;
using Serilog.Events;
using SyncaLogger.Api.Contacts;
using SyncaLogger.Api.Models;
using SyncaLogger.Data.Models;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Tests.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace SyncaLogger.Tests
{
    public class IntegrationTest : DBMethodsForTesting
    {

        [Fact]
        public async Task ReturnUnauthorizedIfPostLogsWithoutApiKeyHeader()
        {
            //
            var expectedStatusCode = System.Net.HttpStatusCode.Unauthorized;

            List<Log> logs = new List<Log>();
            string json = JsonConvert.SerializeObject(logs);

            //
            using (var client = new TestProvider().Client)
            {
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync($"/api/log", data);

                var statusCode = response.StatusCode;

                Assert.Equal(expectedStatusCode, statusCode);
            }
        }

        [Fact]
        public async Task ReturnUnauthorizedIfGetLogByIdWithoutJWT()
        {
            //
            var expectedStatusCode = System.Net.HttpStatusCode.Unauthorized;

            //
            using (var client = new TestProvider().Client)
            {
                var response = await client.GetAsync($"/api/log/1");

                var statusCode = response.StatusCode;

                //
                Assert.Equal(expectedStatusCode, statusCode);
            }
        }

        [Fact]
        public async Task ReturnOkIfGetLogByIdWithJWT()
        {
            //
            var expectedStatusCode = System.Net.HttpStatusCode.OK;

            //
            using (var client = new TestProvider().Client)
            {
                var jwt = await GetTestJwt(client);

                await SeedLogsToDB(client);

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwt);

                var response = await client.GetAsync($"/api/log/1");

                var statusCode = response.StatusCode;

                //
                Assert.Equal(expectedStatusCode, statusCode);
            }
        }


        [Fact]
        public async Task ReturnCreatedIfPostLogsWithRightApiKeyHeader()
        {
            //
            var expectedStatusCode = System.Net.HttpStatusCode.Created;

            List<Log> logs = new List<Log>();
            string json = JsonConvert.SerializeObject(logs);

            //
            using (var client = new TestProvider().Client)
            {
                client.DefaultRequestHeaders.Add("ApiKey", "1144apiSynca11");
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync($"/api/log", data);

                var statusCode = response.StatusCode;

                //
                Assert.Equal(expectedStatusCode, statusCode);
            }
        }
    }
}
