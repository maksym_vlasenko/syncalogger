﻿using Moq;
using SyncaLogger.Data.Models;
using SyncaLogger.Data.Repositories;
using SyncaLogger.Domain.DTOModels;
using SyncaLogger.Domain.Services.Default;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Serilog.Events;
using System.Linq;
using AutoMapper;
using SyncaLogger.Domain;
using SyncaLogger.Domain.Services;
using MockQueryable.Moq;

namespace SyncaLogger.Tests
{
    public class LogServiceTests
    {
        private List<Log> logs;

        private IMapper _iMapper;

        public LogServiceTests()
        {
            logs = new List<Log>()
            {
                new Log() { Id = 1,Text = "Error message 1",Date=DateTime.Now, Level = LogEventLevel.Information},
                new Log() { Id = 2,Text = "Error message 2",Date=DateTime.Now.AddDays(-7), Level = LogEventLevel.Information},
                new Log() { Id = 3,Text = "Error message 3",Date=DateTime.Now.AddDays(-1), Level = LogEventLevel.Fatal },
                new Log() { Id = 4,Text = "Error message 4",Date=DateTime.Now.AddDays(-4), Level = LogEventLevel.Information},
                new Log() { Id = 5,Text = "Error message 5",Date=DateTime.Now.AddDays(-3), Level = LogEventLevel.Warning},
                new Log() { Id = 6,Text = "Error message 6",Date=DateTime.Now.AddDays(-56), Level = LogEventLevel.Error},
                new Log() { Id = 7,Text = "Error message 7",Date=DateTime.Now.AddDays(-22), Level = LogEventLevel.Debug},
            };


            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BisMap>();
            });

            _iMapper = config.CreateMapper();

        }


        [Fact]
        public async Task GetLogByIdReturnNotNull()
        {
            //
            Log logFromRepo = new Log();

            var moq = new Mock<ILogRepository>();
            moq.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(logFromRepo);

            var ser = new LogService(moq.Object, _iMapper);

            //
            var log = await ser.GetLogById(It.IsAny<int>());

            //
            Assert.NotNull(log);
        }

        [Fact]
        public async Task GetLogByIdReturnNullIfIdOutOfRange()
        {
            //
            int outOfRangeId = logs.Count() + 1;

            var mockRepo = new Mock<ILogRepository>();
            mockRepo.Setup(m => m.GetByIdAsync(outOfRangeId))
                .ReturnsAsync(logs.SingleOrDefault(x=>x.Id==outOfRangeId));

            var logService = new LogService(mockRepo.Object, _iMapper);

            //
            var log = await logService.GetLogById(outOfRangeId);

            //
            Assert.Null(log);
        }

        [Theory]
        [InlineData(22)]
        [InlineData(2)]
        [InlineData(55)]
        public async Task GetLogByIdReturnLogWithRightId(int id)
        {
            //
            Log logFromRepo = new Log() { Id = id };

            var moqRepo = new Mock<ILogRepository>();
            moqRepo.Setup(m => m.GetByIdAsync(id))
                .ReturnsAsync(logFromRepo);

            var logService = new LogService(moqRepo.Object, _iMapper);

            //
            var log = await logService.GetLogById(id);

            //
            Assert.Equal(id, log.Id);
        }

        [Theory]
        [InlineData(SyncaLogLevel.Information)]
        [InlineData(SyncaLogLevel.All)]
        [InlineData(SyncaLogLevel.Error)]
        public async Task FindByLogLevelReturnRightCountOfSiutableLogs(SyncaLogLevel expectedLevel)
        {
            //
            int expectedCountOfLogs;

            if (expectedLevel != SyncaLogLevel.All)
            {
                expectedCountOfLogs
                     = logs.Where(x => x.Level == (LogEventLevel)expectedLevel)
                           .Count();
            }
            else
            {
                expectedCountOfLogs = logs.Count();
            }

            SearchFilterDTO searchDTO =
                new SearchFilterDTO()
                {
                    PageOptions = new Pagination()
                    {
                        PageNumber = 1,
                        PageSize = expectedCountOfLogs
                    },
                    LogLevel = expectedLevel,
                    AfterThisDate = DateTime.MinValue,
                    BeforeThisDate = DateTime.Now
                };

            var logsFromRepo = logs.AsQueryable().BuildMock();

            var mockRepo = new Mock<ILogRepository>();
            mockRepo.Setup(m => m.GetQuery())
                .Returns(logsFromRepo.Object);

            var logService = new LogService(mockRepo.Object, _iMapper);

            //
            var logsAfterFilterSearch = await logService.Find(searchDTO);

            //
            Assert.Equal(expectedCountOfLogs, logsAfterFilterSearch.logDTOs.Count());
        }


        [Theory]
        [InlineData(SyncaLogLevel.Information)]
        [InlineData(SyncaLogLevel.Fatal)]
        [InlineData(SyncaLogLevel.Warning)]
        public async Task ReturnOnlyLogsWithRightLogLevel(SyncaLogLevel expectedLevel)
        {
            //
            SearchFilterDTO searchDTO =
                new SearchFilterDTO()
                {
                    PageOptions = new Pagination()
                    {
                        PageNumber = 1,
                        PageSize = logs.Count()
                    },
                    LogLevel = expectedLevel
                };

            var logsFromRepo = logs.AsQueryable().BuildMock();

            var mockRepo = new Mock<ILogRepository>();
            mockRepo.Setup(m => m.GetQuery())
                .Returns(logsFromRepo.Object);

            var logService = new LogService(mockRepo.Object, _iMapper);

            //
            var logsAfterFilterSearch = await logService.Find(searchDTO);

            //
            Assert.True(logsAfterFilterSearch.logDTOs.All(x => x.Level == expectedLevel));
        }


        [Theory]
        [InlineData(-20)]
        [InlineData(-3)]
        [InlineData(-15)]
        [InlineData(-5)]
        [InlineData(-1)]
        public async Task FindByDateReturnRightCountOfSiutableLogs(int daysToAdd)
        {
            //
            var testTime = DateTime.Now.AddDays(daysToAdd);

            int expectedCountOfLogs
                  = logs.Where(x => x.Date > testTime )
                        .Count();

            SearchFilterDTO searchDTO =
                new SearchFilterDTO()
                {
                    PageOptions = new Pagination()
                    {
                        PageNumber = 1,
                        PageSize = expectedCountOfLogs
                    },
                    AfterThisDate = testTime,
                    LogLevel = SyncaLogLevel.All,
                    BeforeThisDate = DateTime.Now
                };

            var logsFromRepo = logs.AsQueryable().BuildMock();

            var mockRepo = new Mock<ILogRepository>();
            mockRepo.Setup(m => m.GetQuery())
                .Returns(logsFromRepo.Object);

            var logService = new LogService(mockRepo.Object, _iMapper);

            //
            var logsAfterFilterSearch = await logService.Find(searchDTO);

            //
            Assert.Equal(expectedCountOfLogs, logsAfterFilterSearch.logDTOs.Count());
        }

        [Fact]
        public async Task AddListOfLogsRunOnce()
        {
            //
            var mockRepo = new Mock<ILogRepository>();
            mockRepo.Setup(m => m.AddRangeAsync(It.IsAny<List<Log>>()));

            var logService = new LogService(mockRepo.Object, _iMapper);

            //
            await logService.SaveRange(It.IsAny<List<LogDTO>>());

            //
            mockRepo.Verify(lw => lw.AddRangeAsync(It.IsAny<List<Log>>()),
            Times.Once());
        }
    }
}
