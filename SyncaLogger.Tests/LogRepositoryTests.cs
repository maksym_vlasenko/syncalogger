﻿using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using Serilog.Events;
using SyncaLogger.Data.EntityFramework;
using SyncaLogger.Data.Models;
using SyncaLogger.Data.Repositories.Default;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace SyncaLogger.Tests
{
    public class LogRepositoryTests
    {
      
        public List<Log> GetTestData()
        {
            var logs = new List<Log>()
            {
                new Log() { Id = 1,Text = "Error message 1",Date=DateTime.Now, Level = LogEventLevel.Information},
                new Log() { Id = 2,Text = "Error message 2",Date=DateTime.Now.AddDays(-7), Level = LogEventLevel.Information},
                new Log() { Id = 3,Text = "Error message 3",Date=DateTime.Now.AddDays(-1), Level = LogEventLevel.Fatal },
                new Log() { Id = 4,Text = "Error message 4",Date=DateTime.Now.AddDays(-4), Level = LogEventLevel.Information},
                new Log() { Id = 5,Text = "Error message 5",Date=DateTime.Now.AddDays(-3), Level = LogEventLevel.Warning},
                new Log() { Id = 6,Text = "Error message 6",Date=DateTime.Now.AddDays(-56), Level = LogEventLevel.Error},
                new Log() { Id = 7,Text = "Error message 7",Date=DateTime.Now.AddDays(-22), Level = LogEventLevel.Debug},
            };
            return logs;
        }

        [Fact]
        public async Task GetLogByIdReturnRightId()
        {
            //
            int expectedId = 1;
            var mockDBSet = GetTestData().AsQueryable().BuildMockDbSet();

            var mockContext = new Mock<SyncaContext>();
            mockContext.Setup(c => c.Logs).Returns(mockDBSet.Object);

            var repo = new LogRepository(mockContext.Object);

            //
            var log = await repo.GetByIdAsync(expectedId);

            //
            Assert.NotNull(log);
            Assert.Equal(expectedId, log.Id);
        }

        [Fact]
        public void GetQueryReturnNotNull()
        {
            //
            var mockDBSet = GetTestData().AsQueryable().BuildMockDbSet();

            var mockContext = new Mock<SyncaContext>();
            mockContext.Setup(c => c.Logs).Returns(mockDBSet.Object);

            var repo = new LogRepository(mockContext.Object);

            //
            var log = repo.GetQuery();

            //
            Assert.NotNull(log);
        }

        [Fact]
        public void GetQueryReturnAllEntities()
        {
            //
            int expectedCountOfLogs = GetTestData().Count();

            var mockDBSet = GetTestData().AsQueryable().BuildMockDbSet();

            var mockContext = new Mock<SyncaContext>();
            mockContext.Setup(c => c.Logs).Returns(mockDBSet.Object);

            var repo = new LogRepository(mockContext.Object);

            //
            var log = repo.GetQuery();

            //
            Assert.Equal(expectedCountOfLogs, log.Count());
        }

    }
}
