﻿using Xunit;
using Moq;
using SyncaLogger.Domain.Services;
using SyncaLogger.Api.Controllers;
using SyncaLogger.Domain.DTOModels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Serilog.Events;
using System.Linq;
using SyncaLogger.Api.Models;
using AutoMapper;
using SyncaLogger.Api.Mapping;
using SyncaLogger.Api.Contacts;

namespace SyncaLogger.Tests
{
    public class LogControllerTest
    {

        private List<LogDTO> logs;
        private IMapper _iMapper;

        public LogControllerTest()
        {
            logs = new List<LogDTO>()
            {
                new LogDTO() { Id = 1,Text = "Error message 1",Date=DateTime.Now, Level = SyncaLogLevel.Information},
                new LogDTO() { Id = 2,Text = "Error message 2",Date=DateTime.Now.AddDays(-10), Level = SyncaLogLevel.Warning },
                new LogDTO() { Id = 3,Text = "Error message 3",Date=DateTime.Now.AddDays(-1), Level = SyncaLogLevel.Fatal },
                new LogDTO() { Id = 4,Text = "Error message 4",Date=DateTime.Now.AddDays(-5), Level = SyncaLogLevel.Information }
            };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<LogMapping>();
            });

            _iMapper = config.CreateMapper();
        }


        [Fact]
        public async Task GetLogByIDReturnOkObjectResult()
        {       
            //
            var mockService = new Mock<ILogService>();

            mockService.Setup(rep => rep.GetLogById(It.IsAny<int>()))
            .ReturnsAsync(new LogDTO());

            var controller = new LogController(null, null, mockService.Object);

            //
            var result = await controller.Get(It.IsAny<int>());

            var okObj = result as OkObjectResult;

            //
            Assert.IsType<OkObjectResult>(okObj);
            
        }

        [Fact]
        public async Task ReturnNoContentResultIfIdOutOfRange()
        {
            //
            int outOfRangeId = logs.Count()+1;
            
            var mockService = new Mock<ILogService>();

            mockService.Setup(rep => rep.GetLogById(outOfRangeId))
            .ReturnsAsync(logs.SingleOrDefault(x=>x.Id==outOfRangeId));

            var controller = new LogController(null, null, mockService.Object);

            //
            var result = await controller.Get(outOfRangeId);

            var noContent = result as NoContentResult;

            //
            Assert.IsType<NoContentResult>(noContent);
        }

        [Fact]
        public async Task Return201StatusCodeAfterSaveLogs()
        {
            //
            int expectedStatusCode = 201;

            var mockService = new Mock<ILogService>();

            mockService.Setup(rep => rep.SaveRange(It.IsAny<List<LogDTO>>()));

            var controller = new LogController(null, _iMapper, mockService.Object);

            //
            var result = await controller.Post(new List<TransferLogModel>());

            var statusCodeResult = result as ObjectResult;

            //
            Assert.Equal(expectedStatusCode, statusCodeResult.StatusCode);
        }

        [Fact]
        public async Task PostMethodReturnBadRequestObjectResultIfModelIsNull()
        {
            //
           List<TransferLogModel> nullModel = null;

            var mockService = new Mock<ILogService>();

            mockService.Setup(rep => rep.SaveRange(logs));

            var controller = new LogController(null, _iMapper, mockService.Object);

            //
            var result = await controller.Post(nullModel);

            var typeOfResult = result as BadRequestObjectResult;

            //
            Assert.IsType<BadRequestObjectResult>(typeOfResult);
        }


        //[Fact]
        //public async Task ReturnBad()
        //{
        //    //
        //    SearchFilter filter = new SearchFilter() { AfterThisDate = DateTime.Now.AddDays(2) };

        //    var mockService = new Mock<ILogService>();

        //    mockService.Setup(rep => rep.Find(It.IsAny<SearchFilterDTO>()))
        //    .ReturnsAsync(logs);


        //    var controller = new LogController(null, null, mockService.Object);

        //    //
        //    var result = await controller.Get(filter);

        //    //
        //    Assert.Null(result.Value);
        //}


    }
}
