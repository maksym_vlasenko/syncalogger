FROM mcr.microsoft.com/dotnet/core/runtime:3.0 AS base
WORKDIR /app
EXPOSE 80
COPY ./publish .
ENV ASPNETCORE_URLS https://+:443;http://+:80
ENTRYPOINT ["dotnet", "SyncaLogger.Api.dll"]


