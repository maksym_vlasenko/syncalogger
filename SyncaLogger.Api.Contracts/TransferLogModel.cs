﻿using Serilog.Events;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SyncaLogger.Api.Contacts
{
    public class TransferLogModel
    {
        public string Text { get; set; }
        public LogEventLevel Level { get; set; }
        public DateTime Date { get; set; }
        public TransferLogModel(string text, LogEventLevel level, DateTime date)
        {
            Text = text;
            Level = level;
            Date = date;
        }
        public TransferLogModel() { }
    }
}
